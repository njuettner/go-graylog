package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

type errorAPI struct {
	Type    string
	Message string
}

type streamsAPI struct {
	Total   int
	Streams []streamAPI
}

type streamAPI struct {
	CreatorUserID   string   `json:"creator_user_id"`
	Outputs         []string `json:"outputs"`
	Description     string
	CreatedAt       string
	Disabled        bool
	Rules           []rulesAPI
	AlertConditions alertAPI
	ID              string
	Title           string
}

type rulesAPI struct {
	Field         string
	StreamID      string `json:"stream_id"`
	ID            string
	Inverted      bool
	Type          int
	CreaterUserID string
}

type alertAPI struct {
	ID            string
	CreatedAt     string
	Parameters    parameterAPI
	Type          string
	creatorUserID string
}

type parameterAPI struct {
	Grace         int
	Time          string
	Backlog       int
	ThresholdType string
	Threshold     int
}

var (
	url      *string
	user     *string
	password *string
)

func init() {
	url = flag.String("url", "http://localhost:12900/streams", "Graylog API URL")
	user = flag.String("user", "graylog", "Graylog API user")
	password = flag.String("password", "graylog", "Graylog API password")
	flag.Parse()
}

func getStreams() {
	var streams streamsAPI
	req, err := http.NewRequest("GET", *url, nil)
	req.SetBasicAuth(*user, *password)
	if err != nil {
		fmt.Printf("Authorization is invalid, Reason: %s", err)
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("Can't contact Page, Reason: %s", err)
	}
	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("Could not read response body, Reason: %s", err)
	}
	json.Unmarshal(content, &streams)
	enc := json.NewEncoder(os.Stdout)
	enc.Encode(streams)
}

func main() {
	if !(len(flag.Args()) > 0) {
		flag.Usage()
		os.Exit(1)
	}
	getStreams()
}
